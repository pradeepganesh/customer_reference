# -*- coding: utf-8 -*-
# Part of Moltis . See LICENSE file for full copyright and licensing details.

{
    'name': 'Partner Referereres',
    'version': '12.0',
    'category': 'CRM',
    'description': """
Partner References
    """,
    'depends': ['contacts'],
    'data': [
        'view/customer_ref_view.xml',
    ],
}
